import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class JSoupHandler
{
	public Elements getElements(String source)
	{			
	    Document doc = null;
		try
		{
			doc = Jsoup.connect(source).get();
		} catch (IOException exception) { /* exception.printStackTrace(); */ }
		
		if (doc != null)
		{
			Elements links = doc.select("table");
			links.clear();
			
			Element table = doc.select("table").get(0);
			Elements rows = table.select("tr");
			System.out.println(rows.size());
			
			for (int i = 3; i < rows.size() - 1; i++)
			{
				Element row = rows.get(i);
				Elements cols = row.select("td");
				Element name = cols.get(1);
				Element lastModified = cols.get(2);
				links.add(cols.get(1));
				links.add(cols.get(2));
			}

		    return links;
		}
		else { return null; }
	}
	
	public boolean checkPageExists(String source)
	{
		Document doc = null;
		try
		{
			doc = Jsoup.connect(source).get();
		} catch (IOException exception) { /* exception.printStackTrace(); */ }
		
		return doc != null;
	}
}