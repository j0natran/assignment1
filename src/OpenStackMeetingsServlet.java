import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.ListIterator;
import java.util.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class OpenStackMeetingsServlet extends HttpServlet
{
	private static final String TAG = "MyServlet";
	private JSoupHandler mJSoupHandler;
	private ArrayList<String> mVisitedURLs;
	private PrintWriter mPrintWriter;
	private Cookie mCookie;
	private String mURLData;
	
	public OpenStackMeetingsServlet()
	{
		mJSoupHandler = new JSoupHandler();
		mVisitedURLs = new ArrayList<String>();
		mPrintWriter = null;
		mCookie = null;
		mURLData = null;
	}
	
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
		System.out.println(TAG + ".doGet()");
		mPrintWriter = response.getWriter();
		mCookie = getCookie(request.getCookies());
		mURLData = "";
		
		String requestURL = request.getRequestURL().toString();
		String requestQuery = URLDecoder.decode(request.getQueryString(), "UTF-8");
		
		String pSession = request.getParameter("session");
		if (pSession != null)
		{
			if (pSession.equals("start"))
			{
				if (mCookie == null)
				{
					createCookie(request, response);
				}
				printVisitedURLs();
				mPrintWriter.println();
				printURLData();
				addVisitedURL(requestURL, requestQuery);
			}
			else if (pSession.equals("end"))
			{
				if (mCookie != null)
				{
					clearCookie(request, response);
				}
				printVisitedURLs();
				mPrintWriter.println();
				printURLData();
				clearVisitedURLs();
			}
		}
		else
		{
			String pProject = request.getParameter("project");
			String pYear = request.getParameter("year");
			printVisitedURLs();
			mPrintWriter.println();
			
			if (pProject == null || pYear == null)
			{
				if (pProject == null)
				{
					if (mCookie != null)
					{
						addVisitedURL(requestURL, requestQuery);
					}
					mPrintWriter.println("Required parameter project missing");
				}
				if (pYear == null)
				{
					if (mCookie != null)
					{
						addVisitedURL(requestURL, requestQuery);
					}
					mPrintWriter.println("Required parameter year missing");
				}
			}
			else
			{
				String source = "http://eavesdrop.openstack.org/meetings/";
				String projectCheck = source + pProject;
				String yearCheck = source + pProject + "/" + pYear;
				mURLData = getURLData(pProject, pYear);
				
				
				if (mJSoupHandler.checkPageExists(projectCheck) == false)
				{
					mPrintWriter.println("Project " + pProject + " not found");
				}
				else if (mJSoupHandler.checkPageExists(yearCheck) == false)
				{
					mPrintWriter.println("Invalid year " + pYear + " for project " + pProject);
				}
				else
				{
					printURLData();
				}
				
				if (mCookie != null)
				{
					addVisitedURL(requestURL, requestQuery);
				}
			}
		}
    }

	private Cookie getCookie(Cookie[] cookies)
	{
		System.out.println(TAG + ".getCookie()");
		if (cookies == null)
		{
			return null;
		}
		
		for (Cookie cookie : cookies)
		{
			String cookieName = cookie.getName();
    		String cookieValue = cookie.getValue();
    		
    		if ((cookieName != null && cookieName.equals("hello")) &&
    			(cookieValue != null && cookieValue.equals("world")))
    		{
    			return cookie;
    		}
		}
		
		return null;
	}
	
	private void createCookie(HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(TAG + ".createCookie()");
		Cookie cookie = new Cookie("hello", "world");
		cookie.setDomain("localhost");
		cookie.setPath("/assignment1" + request.getServletPath());
		cookie.setMaxAge(1000);
		response.addCookie(cookie);
	}
	
	private void clearCookie(HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(TAG + ".clearCookie()");
		Cookie cookie = new Cookie("hello", "world");
		cookie.setDomain("localhost");
		cookie.setPath("/assignment1" + request.getServletPath());
		cookie.setMaxAge(0);
		response.addCookie(cookie);
	}
	
	private void addVisitedURL(String requestURL, String requestQuery)
	{
		System.out.println(TAG + ".addVisitedURL()");
		String visitedURL = requestURL;			
		if (requestQuery != null)
		{
			visitedURL += "?" + requestQuery;
		}		
		mVisitedURLs.add(visitedURL);
	}
	
	private void printVisitedURLs()
	{
		System.out.println(TAG + ".printVisitedURLs()");
		mPrintWriter.println("Visited URLs:");
		for (String visitedURL : mVisitedURLs)
		{
			mPrintWriter.println(visitedURL);
		}
	}
	
	private void clearVisitedURLs()
	{
		System.out.println(TAG + ".clearVisitedURLs()");
		mVisitedURLs = new ArrayList<String>();
	}
	
	private String getURLData(String project, String year)
	{
		StringBuilder URLData = new StringBuilder();
		try
		{
			String source = "http://eavesdrop.openstack.org/meetings/" + project + "/" + year;	
			Elements links = mJSoupHandler.getElements(source);
	
		    if (links != null)
		    {
			    ListIterator<Element> iter = links.listIterator();		    	
			    while(iter.hasNext())
			    {
		    			Element e = (Element) iter.next();
		    			String s = e.text();
		    			if (s != null) 
		    			{
		    				URLData.append(s + " ");
		    			}
		    			
		    			if (iter.hasNext())
		    			{
		    				e = (Element) iter.next();
		    				s = e.text();
		    				if (s != null)
		    				{
		    					URLData.append(s + "\n");
		    				}
		    			}
			    }
		    }
		    else
		    {
		    		URLData.append("Unknown project " + project);
		    }
		} catch (Exception exception) { /* exception.printStackTrace(); */ }
		
		return URLData.toString();
	}
	
	private void printURLData()
	{
		mPrintWriter.println("URL Data:");
		mPrintWriter.println(mURLData);
	}
}